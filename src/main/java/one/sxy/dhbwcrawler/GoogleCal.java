package one.sxy.dhbwcrawler;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.EventDateTime;
import one.sxy.dhbwcrawler.property.Injector;
import one.sxy.dhbwcrawler.property.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

public class GoogleCal {
    private static final Logger log = LoggerFactory.getLogger(GoogleCal.class);

    @Property("google.calendarKey")
    private static String CALENDAR_ID = "666obp6ro6slnc0346ol54vook@group.calendar.google.com";
    @Property("google.applicationName")
    private static String APPLICATION_NAME = "dhbw-inf17c";


    private static final File DATA_STORE_DIRECTORY = new File("credentials");
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
    public static Calendar service;
    private static FileDataStoreFactory DATA_STORE_FACTORY;
    private static HttpTransport HTTP_TRANSPORT;

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIRECTORY);
            service = getCalendarService();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public GoogleCal() throws IllegalAccessException {
        Injector.initialize(this);
    }

    /**
     * Creates an authorized Credential object.
     *
     * @return an authorized Credential object.
     * @throws IOException
     */
    private static Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in =
                Main.class.getResourceAsStream("/client_secret.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIRECTORY.getAbsolutePath());
        return credential;
    }

    /**
     * Build and return an authorized Calendar client service.
     *
     * @return an authorized Calendar client service
     * @throws IOException
     */
    private static com.google.api.services.calendar.Calendar
    getCalendarService() throws IOException {
        Credential credential = authorize();
        return new com.google.api.services.calendar.Calendar.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public String addToGoogle(Event event) throws IOException {
        com.google.api.services.calendar.model.Event gEvent = new com.google.api.services.calendar.model.Event();

        EventDateTime start = new EventDateTime();
        start.setDateTime(new DateTime(event.getStartUnix() * 1000L));
        start.setTimeZone("Europe/Berlin");
        gEvent.setStart(start);

        EventDateTime end = new EventDateTime();
        end.setDateTime(new DateTime(event.getEndUnix() * 1000L));
        end.setTimeZone("Europe/Berlin");
        gEvent.setEnd(end);

        gEvent.setSummary(event.getName());

        gEvent.setLocation(event.getLocation());

        String description = "";
        if (event.getRoom() != null) {
            description = "Raum: " + event.getRoom() + " ";
        }
        if (event.getDozent() != null) {
            description = description + "Dozent: " + event.getDozent();
        }
        gEvent.setDescription(description);

        return service.events().insert(CALENDAR_ID, gEvent)
                .execute().getId();
    }

    public boolean removeFromGoogle(String id) throws IOException {
        return removeFromGoogle(id, 0);
    }

    public boolean removeFromGoogle(String id, int counter) throws IOException {
        try {
            service.events().delete(CALENDAR_ID, id).execute();

            try {
                GoogleCal.service.events()
                        .get(CALENDAR_ID, id).execute();
                if(counter < 3){
                    return removeFromGoogle(id, counter+1);
                }
                return false;
            } catch (GoogleJsonResponseException e) {
                if (e.getDetails().getCode() == 404) {
                    return true;
                } else {
                    throw e;
                }
            }
        } catch (GoogleJsonResponseException e) {
            if (e.getDetails().getCode() == 410) {
                return true;
            }
            log.error("Could not delete entry with reason " + e.getDetails().getMessage() + " " + e.getDetails().getCode());
            return false;
        }
    }
}
