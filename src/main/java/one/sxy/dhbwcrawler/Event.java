package one.sxy.dhbwcrawler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Event {
    private SimpleDateFormat dateFormatter = new SimpleDateFormat();

    private String type;

    private int id;
    private String name;
    private Calendar start;
    private Calendar end;
    private String location;
    private String googleid;
    private String room;
    private String dozent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(long start) {
        if (start != 0L) {
            Calendar toSet = new GregorianCalendar();
            toSet.setTimeInMillis(start * 1000L);
            this.start = toSet;
        }
    }

    public void setStart(Calendar start) {
        this.start = (Calendar) start.clone();
    }

    public String getStart(String format) {
        dateFormatter.applyPattern(format);
        return dateFormatter.format(start.getTime());
    }

    public long getStartUnix() {
        if (start != null) {
            return (start.getTimeInMillis() / 1000L);
        }
        return 0L;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(long start) {
        if (start != 0L) {
            Calendar toSet = new GregorianCalendar();
            toSet.setTimeInMillis(start * 1000L);
            this.end = toSet;
        }
    }

    public void setEnd(Calendar end) {
        this.end = (Calendar) end.clone();
    }

    public String getEnd(String format) {
        dateFormatter.applyPattern(format);
        return dateFormatter.format(end.getTime());
    }

    public long getEndUnix() {
        if (end != null) {
            return (end.getTimeInMillis() / 1000L);
        }
        return 0L;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGoogleid() {
        return googleid;
    }

    public void setGoogleid(String googleid) {
        this.googleid = googleid;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDozent() {
        return dozent;
    }

    public void setDozent(String dozent) {
        this.dozent = dozent;
    }

    @Override
    public String toString() {
        StringBuilder bla = new StringBuilder();
        bla.append("Name: ").append(name);
        if (start != null) {
            bla.append(", Start: ").append(getStart("dd.MM.yyyy HH:mm"));
        }
        if (end != null) {
            bla.append(", Ende: ").append(getEnd("dd.MM.yyyy HH:mm"));
        }
        return bla.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (!getName().equals(event.getName())) return false;
        if (!getStart().equals(event.getStart())) return false;
        if (!getEnd().equals(event.getEnd())) return false;
        if (getLocation() != null ? !getLocation().equals(event.getLocation()) : event.getLocation() != null)
            return false;
        if (getRoom() != null ? !getRoom().equals(event.getRoom()) : event.getRoom() != null) return false;
        return getDozent() != null ? getDozent().equals(event.getDozent()) : event.getDozent() == null;
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getStart().hashCode();
        result = 31 * result + getEnd().hashCode();
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getDozent() != null ? getDozent().hashCode() : 0);
        return result;
    }

    //    @Override
//    public boolean equals(Object obj) {
//        if (obj.getClass() != this.getClass()) {
//            return false;
//        }
//        Event event = (Event) obj;
//        //first check if it's the same object
//        return (super.equals(obj) ||
//                // check if name is the same
//                (name.equalsIgnoreCase(event.getName()) &&
//                        // check if start is the same
//                        ((start == null && event.getStart() == null) ||
//                                (start != null && event.getStart() != null && start.equals(event.getStart()))) &&
//                        // check if end is the same
//                        ((end == null && event.getEnd() == null) ||
//                                (end != null && event.getEnd() != null && end.equals(event.getEnd()))) &&
//                        // check if location is the same
//                        ((location == null && event.getLocation() == null) ||
//                                (location != null && event.getLocation() != null
//                                        && location.equalsIgnoreCase(event.getLocation()))) &&
//                        ((room == null && event.getRoom() == null) ||
//                                (room != null && event.getRoom() != null && room.equalsIgnoreCase(event.getRoom()))) &&
//                        ((dozent == null && event.getDozent() == null) ||
//                                (dozent != null && event.getDozent() != null && dozent.equalsIgnoreCase(event.getDozent())))));
//    }
}
