package one.sxy.dhbwcrawler.property;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFile {
    private final static Logger log = LoggerFactory.getLogger(PropertiesFile.class);
    private static Properties properties = new Properties();

    static {
        try {
            properties.load(ClassLoader.getSystemClassLoader().getResourceAsStream("crawler.properties"));
            log.info("Loaded properties file: " + properties.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(123123123);
        }
    }

    public static int getInt(String name) {
        return Integer.parseInt(properties.getProperty(name));
    }

    public static int getInt(String name, Integer defaultInt) {
        return Integer.parseInt(properties.getProperty(name, defaultInt.toString()));
    }

    public static String getString(String name) {
        return properties.getProperty(name);
    }

    public static String getString(String name, String defaultString) {
        return properties.getProperty(name, defaultString);
    }
}
