package one.sxy.dhbwcrawler.property;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class Injector {
    private static final Logger log = LoggerFactory.getLogger(Injector.class);

    public static void initialize(Object instance) throws IllegalAccessException {
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Property.class)) {
                Property set = field.getAnnotation(Property.class);

                field.setAccessible(true); // should work on private fields
                try {
                    String withDefault[] = set.value().split(":");

                    if (set.type().equalsIgnoreCase("string")) {
                        if (withDefault.length == 1) {
                            field.set(instance, PropertiesFile.getString(withDefault[0]));
                        } else if (withDefault.length == 2) {
                            field.set(instance, PropertiesFile.getString(withDefault[0], withDefault[1]));
                        } else {
                            log.error("you should not use \":\" in your properties name or default value");
                        }
                    } else if (set.type().equalsIgnoreCase("int")) {
                        if (withDefault.length == 1) {
                            field.setInt(instance, PropertiesFile.getInt(withDefault[0]));
                        } else if (withDefault.length == 2) {
                            field.setInt(instance, PropertiesFile.getInt(withDefault[0], Integer.parseInt(withDefault[1])));
                        } else {
                            log.error("you should not use \":\" in your properties name or default value");
                        }
                    } else {
                        log.error("specified field of type " + set.type() + "but only \"string\" and \"int\" are supported right now");
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
        }
    }
}
