package one.sxy.dhbwcrawler;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import one.sxy.dhbwcrawler.property.Injector;
import one.sxy.dhbwcrawler.property.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Database {
    private static final Logger log = LoggerFactory.getLogger(Database.class);

    @Property("database.user")
    private static String USER;
    @Property("database.password")
    private static String PASSWORD;
    @Property("database.host")
    private static String HOST;
    @Property(value = "database.port", type = "int")
    private static int PORT;
    @Property("database.database")
    private static String DATABASE;

    static {
        try {
            Injector.initialize(Database.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement create;
    private PreparedStatement getall;
    private PreparedStatement getallbetween;
    private PreparedStatement delete;

    public Database() throws SQLException, IllegalAccessException {
        Injector.initialize(this);

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE);
        config.setUsername(USER);
        config.setPassword(PASSWORD);
        HikariDataSource ds = new HikariDataSource(config);
        Connection con = ds.getConnection();
        create = con.prepareStatement("INSERT INTO events (eventname, starttime, endtime, location, googleid, " +
                "room, dozent) VALUES (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        getall = con.prepareStatement("SELECT id, eventname, starttime, endtime, location, googleid, room," +
                "dozent FROM events");
        getallbetween = con.prepareStatement("SELECT id, eventname, starttime, endtime, location, googleid, " +
                "room, dozent FROM events WHERE starttime BETWEEN ? AND ? OR starttime = 0");
        delete = con.prepareStatement("DELETE FROM events WHERE id = ?");
    }

    public int saveEvent(Event event) throws SQLException {
        create.setString(1, event.getName());
        create.setLong(2, event.getStartUnix());
        create.setLong(3, event.getEndUnix());
        create.setString(4, event.getLocation());
        create.setString(5, event.getGoogleid());
        create.setString(6, event.getRoom());
        create.setString(7, event.getDozent());
        int affected = create.executeUpdate();
        create.clearParameters();
        if (affected != 1) {
            throw new SQLException("Wrong number of lines changed");
        }
        ResultSet ids = create.getGeneratedKeys();
        if (!ids.next()) {
            throw new SQLException("Could not get ID");
        }
        return ids.getInt(1);
    }

    public EventList getAll() throws SQLException {
        EventList list = new EventList();

        ResultSet all = getall.executeQuery();

        while (all.next()) {
            list.add(mapResultToEvent(all));
        }

        return list;
    }

    public EventList getAllBetween(long start, long end) throws SQLException {
        EventList list = new EventList();

        getallbetween.setLong(1, start);
        getallbetween.setLong(2, end);

        ResultSet all = getallbetween.executeQuery();

        log.info("SELECT id, eventname, starttime, endtime, location, googleid, " +
                "room, dozent FROM events WHERE starttime BETWEEN " + start +
                " AND " + end + " OR starttime = 0");

        while (all.next()) {
            list.add(mapResultToEvent(all));
        }

        return list;
    }

    public void deleteId(int id) throws SQLException {
        delete.setInt(1, id);
        int changed = delete.executeUpdate();
        delete.clearParameters();
        if (changed != 1) {
            throw new SQLException("Deleted " + changed + " rows!");
        }
    }

    private Event mapResultToEvent(ResultSet result) throws SQLException {
        Event event = new Event();
        event.setName(result.getString("eventname"));
        event.setStart(result.getLong("starttime"));
        event.setEnd(result.getLong("endtime"));
        event.setLocation(result.getString("location"));
        event.setId(result.getInt("id"));
        event.setGoogleid(result.getString("googleid"));
        event.setRoom(result.getString("room"));
        event.setDozent(result.getString("dozent"));
        return event;
    }
}
