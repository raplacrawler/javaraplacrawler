package one.sxy.dhbwcrawler;

import one.sxy.dhbwcrawler.property.Injector;
import one.sxy.dhbwcrawler.property.Property;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class DHBWCal {
    private final static Logger log = LoggerFactory.getLogger(DHBWCal.class);
    private final static long dayInMillis = 60L * 60L * 24L * 1000L;

    @Property("dhbw.location")
    private static String LOCATION;
    @Property("dhbw.calendarKey")
    private static String KEY;
    @Property("dhbw.courseName")
    private static String COURSE;

    private Database db;
    private GoogleCal cal;

    public DHBWCal(Database db, GoogleCal cal) throws IllegalAccessException {
        Injector.initialize(this);

        this.db = db;
        this.cal = cal;
    }

    private static String getValue(Elements infos, String query) {
        for (Element info : infos) {
            List<String> keyValue = new ArrayList<>();
            Elements pair = info.children();
            for (Element value : pair) {
                keyValue.add(value.html());
            }
            if (keyValue.get(0).equalsIgnoreCase(query)) {
                return keyValue.get(1);
            }
        }
        return null;
    }

    public void syncWeek(long startLong, long endLong) throws SQLException, IOException {
        Calendar today = new GregorianCalendar();
        today.setTimeInMillis(startLong * 1000L);

        int date = today.get(Calendar.DATE);
        int month = today.get(Calendar.MONTH) + 1;
        int year = today.get(Calendar.YEAR);

        EventList all = db.getAllBetween(startLong, endLong);

        String url = "https://rapla.dhbw-stuttgart.de/rapla?key=" + KEY + "&day=" + date +
                "&month=" + month + "&year=" + year;

        log.info(url);
        Document doc = Jsoup.connect(url).get();

        Elements tooltips = doc.select(".tooltip");
        for (Element tooltip : tooltips) {
            Elements children = tooltip.children();
            Event actual = htmlToEvent(children, today);
            if (!actual.getType().equalsIgnoreCase("feiertag")
//                    && !actual.getType().equalsIgnoreCase("klausur")
                    ) {
                int id;
                if ((id = all.find(actual)) == -1) {
                    String googleid = cal.addToGoogle(actual);
                    actual.setGoogleid(googleid);
                    db.saveEvent(actual);
                    log.info("Created: " + actual.toString());
                } else {
                    all.remove(id);
                }
            }
        }

        for (Event toDelete : all.getList()) {
            if (cal.removeFromGoogle(toDelete.getGoogleid())) {
                db.deleteId(toDelete.getId());
                log.info("Deleted: " + toDelete.toString());
            } else {
                log.error("Deletion of: " + toDelete.toString() + " failed!");
            }
        }
    }

    private Event htmlToEvent(Elements children, Calendar today) {
        Event actual = new Event();

        actual.setLocation(LOCATION);

        for (Element child : children) {
            if (child.is("table")) {
                Elements infos = child.select("tr");
                actual.setName(getValue(infos, "Veranstaltungsname:"));

                String origResourcen = getValue(infos, "Ressourcen:");
                String resourcen = origResourcen;
                if (resourcen != null) {
                    String todaysDateAsString = actual.getStart("dd.MM.yy");
                    int indexOfToday = resourcen.indexOf(todaysDateAsString);
                    if (indexOfToday != -1) {
                        int indexOfSmallBeforeTodaysIndex = resourcen.lastIndexOf("<small>", indexOfToday);
                        int indexOfCommaBeforeSmallIndex = (resourcen.lastIndexOf(",", indexOfSmallBeforeTodaysIndex) == -1) ? 0 : resourcen.lastIndexOf(",", indexOfSmallBeforeTodaysIndex);
                        String room = resourcen.substring(indexOfCommaBeforeSmallIndex, indexOfSmallBeforeTodaysIndex);
                        if(room.startsWith(",")) room = room.substring(1);
                        actual.setRoom(room);
                    } else {
                        resourcen = resourcen.replaceAll("<small> *\\([A-Za-z0-9 .:,]*\\) *<\\/small>", "");
                        resourcen = resourcen.replaceAll(COURSE, "");
                        if (!"".equals(resourcen)) {
                            resourcen = (resourcen.charAt(0) == ',') ? resourcen.substring(1) : resourcen;
                            resourcen = (resourcen.charAt(resourcen.length() - 1) == ',') ? resourcen.substring(0, resourcen.length() - 1) : resourcen;
                            if (resourcen.contains(",")) {
                                log.info(origResourcen + " does not contain a format where I can get a room from");
                            } else {
                                actual.setRoom(resourcen);
                            }
                        }
                    }
                }

                actual.setDozent(getValue(infos, "Personen:"));
            } else if (child.is("div") && !child.attributes().hasKeyIgnoreCase("style")) {
                String dateString = child.html();
                String[] splited = dateString.split(" ");
                if (splited[1].equalsIgnoreCase("täglich")) {
                    //TODO: Überlegen wie man diesen Scheiss abfangen kann
                } else if (splited[2].equalsIgnoreCase("wöchentlich")) {
                    long toAdd = 0L;
                    if (splited[0].equalsIgnoreCase("di")) {
                        toAdd = dayInMillis;
                    } else if (splited[0].equalsIgnoreCase("mi")) {
                        toAdd = 2L * dayInMillis;
                    } else if (splited[0].equalsIgnoreCase("do")) {
                        toAdd = 3L * dayInMillis;
                    } else if (splited[0].equalsIgnoreCase("fr")) {
                        toAdd = 4L * dayInMillis;
                    } else if (splited[0].equalsIgnoreCase("sa")) {
                        toAdd = 5L * dayInMillis;
                    } else if (splited[0].equalsIgnoreCase("so")) {
                        toAdd = 6L * dayInMillis;
                    }
                    Calendar current = (Calendar) today.clone();
                    current.setTimeInMillis(current.getTimeInMillis() + toAdd);
                    String splitedTime[] = splited[1].split("-");
                    actual.setStart(setTime(current, splitedTime[0]));
                    actual.setEnd(setTime(current, splitedTime[1]));
                } else {
                    String timeSplited[] = splited[2].split("-");

                    actual.setStart(extractDate(splited[1], timeSplited[0]));
                    actual.setEnd(extractDate(splited[1], timeSplited[1]));
                }
            } else if (child.is("strong")) {
                actual.setType(child.html());
            }
        }
        return actual;
    }

    private Calendar extractDate(String date, String time) {
        String dateSplited[] = date.split("\\.");
        String timeSplited[] = time.split(":");

        return new GregorianCalendar((2000 + Integer.parseInt(dateSplited[2])),
                (Integer.parseInt(dateSplited[1]) - 1), Integer.parseInt(dateSplited[0]),
                Integer.parseInt(timeSplited[0]), Integer.parseInt(timeSplited[1]));
    }

    private Calendar setTime(Calendar toSet, String time) {
        String timeSplited[] = time.split(":");
        toSet.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeSplited[0]));
        toSet.set(Calendar.MINUTE, Integer.parseInt(timeSplited[1]));
        return toSet;
    }
}
