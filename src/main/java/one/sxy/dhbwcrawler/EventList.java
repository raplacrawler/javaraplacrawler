package one.sxy.dhbwcrawler;

import java.util.ArrayList;
import java.util.List;

public class EventList {
    private List<Event> list = new ArrayList<>();

    public void add(Event event) {
        list.add(event);
    }

    public int find(Event event) {
        for (Event actual : list) {
            if (actual.equals(event)) {
                return actual.getId();
            }
        }
        return -1;
    }

    public void remove(int id) {
        for (Event actual : list) {
            if (actual.getId() == id) {
                list.remove(actual);
                return;
            }
        }
    }

    public List<Event> getList() {
        return list;
    }

    @Override
    public String toString() {
        StringBuilder all = new StringBuilder();
        for (Event event : list) {
            all.append(event.toString());
            all.append(", ");
        }
        return all.toString();
    }
}
