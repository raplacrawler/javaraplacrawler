package one.sxy.dhbwcrawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {
    private final static Logger log = LoggerFactory.getLogger(Main.class);
    private final static long weekInMillis = 60L * 60L * 24L * 7L * 1000L;
    private final static long dayInMillis = 60L * 60L * 24L * 1000L;

    public static void main(String... args) throws IOException, SQLException, IllegalAccessException {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();

        Database db = new Database();
        GoogleCal cal = new GoogleCal();
        DHBWCal dhbwCal = new DHBWCal(db, cal);

        //Initialize Today and set it to start of week
        Calendar today = new GregorianCalendar();

        today.set(Calendar.HOUR_OF_DAY, 2);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.DAY_OF_WEEK, 2);
        today.setTimeInMillis(today.getTimeInMillis() - 2 * weekInMillis);
        //Finished initializing of date and timestamps

        for (int i = 0; i < 20; i++) {
            long startLong = today.getTimeInMillis() / 1000L;

            today.setTimeInMillis(today.getTimeInMillis() + weekInMillis);
            long endLong = today.getTimeInMillis() / 1000L;

            dhbwCal.syncWeek(startLong, endLong);
        }
    }
}
